# ПОДГОТОВКА СТЕНДА

## 1. Creating a cluster with kubeadm (k8s)
dev стенд кластер (условно он же prod) на 3-х машинах (дистрибутив ubuntu 22.04) - master, worker1, worker2, в директории k8s_cluster лежат ansible-playbook и bash скрипт для запуска
- на машинах кластера должны быть ssh ключи (для доступа с машины где запускается ansible-playbook)
- в файле k8s_cluster/hosts прописаны ip адреса машин кластера <ip_adress>

```console
[all_servers:vars]
ip_master=<ip_adress>
ip_worker1=<ip_adress>
ip_worker2=<ip_adress>
```
- в файле ~/.ssh/config прописаны ip адреса машин кластера 
￼
- запуск bash скрипта для создания кластера из директории k8s_cluster

```console
./startup.sh
```
- скрипт на выходе дает два токена от ServiceAccount (dev, prod) условно в разных namespace (developer, production), шаги описаны в k8s_cluster/6_create_serviceaccount.yml


## 2. Creating a gitlab runer (docker in docker)

- установлен GitLab Runner (на отдельной машине) согластно документации https://docs.gitlab.com/runner/install/docker.html#option-2-use-docker-volumes-to-start-the-runner-container (default Docker image == docker:dind, tags==gitlab) 

- для решения ошибки прокинут сокет внутрь контейнера: строка volumes = ["/cache"] заменена на  volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]



## 3. Creating a gitlab variables
- подготовка переменных в файле .gitlab-ci.yml
```console
K8S_DEV_API_URL: https://<ip_master>:6443         - ip master dev cluster in .gitlab-ci.yml
K8S_PROD_API_URL: https://<ip_master>:6443        - ip master prod cluster in .gitlab-ci.yml
```
- подготовка стандартных переменных gitlab (с опцией masked)
```console
K8S_DEV_CI_TOKEN=<token-dev>                     - токен получен на выводе скрипта создания кластера
K8S_PROD_CI_TOKEN=<token-prod>                   - токен получен на выводе скрипта создания кластера
POSTGRES_PASSWORD_PROD=<password-postgres-prod>
```
## 4. Result
![result.png](result.png)